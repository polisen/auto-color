import styled from "styled-components";
import Image from "next/image";
import { Palette } from "color-thief-react";

import React, { useCallback, useState, useEffect } from "react";
import { useDropzone } from "react-dropzone";
import { getComplements } from "utility/getComplementaryColors";

const Dot = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 100%;
  background-color: ${({ color }) => color};
  margin: 2px;
`;

const ImageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #121212;
  flex-direction: column;
  margin: 10px;
`;

const ColorContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const DotsContainer = styled(ColorContainer)`
  flex-direction: row;
`;
const StyledImage = styled(Image)`
  width: 100px;
  height: 100px;
  border-radius: 8px;
`;

const Dots = ({ url, setPalette, colorCount = 4, display = true}: any) => {
  return (
    <DotsContainer>
      <Palette colorCount={colorCount} format={"hslString"} src={url}>
        {({ data, loading, error }) => {
          if (!loading && data) {
            setPalette(data);
            return display ? data.map((color) => <Dot color={color} />) : <></>
          }
        }}
      </Palette>
    </DotsContainer>
  );
};

const JustDots = ({data}: {data: string[]}) => {
  return (
    <DotsContainer>
    {data.map((color) => <Dot color={color} />)}

  </DotsContainer>
  )
}

const ImageWithPalette = ({
  url,
  next,
}: {
  url: string;
  next: { url: string };
}) => {
  const [PaletteData, setPaletteData]: [string[], Function] = useState([]);
  const [nextPaletteData, setNextPaletteData]: [string[], Function] = useState(
    []
  );
  const [method, setMethod] = useState('complement');
  const [combined, setCombined]: [string[], Function]  = useState([]);
  const [Complements, setComplements]: any = useState([]);
  useEffect(() => {
    // console.debug({PaletteData});
    PaletteData.length > 0 && nextPaletteData.length > 0 && setCombined([...PaletteData, ...nextPaletteData])
  }, [PaletteData, nextPaletteData]);

  useEffect(() => {
    combined.length > 0 && setComplements(getComplements(combined, method))
  }, [combined]);
  return (
    <ImageContainer>
      <ColorContainer>
      {/* palette */}
        <Dots url={url} setPalette={setPaletteData} />
        <StyledImage src={url} height={"100px"} width={"100px"} />
        {/* next */}
        <Dots url={next.url} setPalette={setNextPaletteData} display={false}/>
        {/* combined
        <JustDots data={combined.length > 0 ? combined : []}/> */}
      </ColorContainer>
      clustered:
      {Complements && <JustDots data={Complements}/>}
    </ImageContainer>
  );
};

export default function Home() {
  const [Files, setFiles] = useState([]);
  const onDrop = useCallback((acceptedFiles: any) => {
    setFiles(
      acceptedFiles
        .filter((f: any) => f.type.includes("image"))
        .map((img: File) => {
          return { ...img, url: URL.createObjectURL(img) };
        })
    );
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      {isDragActive ? (
        <p>Drop the files here ...</p>
      ) : (
        <p>Drag 'n' drop some files here, or click to select files</p>
      )}

      {Files &&
        Files.map(({ url }, index) => (
          <ImageWithPalette url={url} next={Files[index + 1] ?? {}} />
        ))}
    </div>
  );
}
