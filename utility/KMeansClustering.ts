import stat from "./statFunctions";
import silhouette from "./silhouette";
function randomBetween(min: number, max: number) {
  return Math.floor(Math.random() * (max - min) + min);
}

const getInitalCentroids = (dataset: number[], K: number): number[] => {
  var arr = [];
  while (arr.length < K) {
    var r = dataset[randomBetween(0, dataset.length)];
    if (arr.indexOf(r) === -1) arr.push(r);
  }
  return arr;
};

const summer = (p: number, c: number) => p + c;

function getNewCentroids(clusters: number[][], centroids: number[]) {
  return centroids.map((c, i) => {
    if (clusters[i]) {
      let sum = Math.floor(clusters[i].reduce(summer));
      let n = clusters[i].length;

      return Math.floor(sum / n);
    } else return c;
  });
}

function same(arr1: number[], arr2: number[]) {
  let condition = true;
  arr1.every((c, i) => {
    if (arr2[i] !== c) {
      condition = false;
      return condition;
    }
    return condition;
  });
  return condition;
}

const MAX_ITERATIONS = 50;

function modulo(x: number, y: number) {
  var xPrime = x;
  while (xPrime < 0) {
    xPrime += y; // ASSUMES y > 0
  }
  return xPrime % y;
}

function KMeansRecurse({
  dataset,
  centroids,
  oldCentroids = [],
  iterations = 1,
}: {
  dataset: number[];
  centroids: number[];
  oldCentroids?: number[];
  iterations?: number;
}): number[][] {
  const clusters: number[][] = [];
  dataset.forEach((h) => {
    const centroidIndex = centroids
      .map((c, i) => {
        let distance = Math.abs(modulo(c, 360) - modulo(h, 360));
        return [distance, i];
      })
      .sort((a: any, b: any) => a[0] - b[0])[0][1];
    if (!clusters[centroidIndex]) clusters[centroidIndex] = [];
    clusters[centroidIndex].push(h);
  });
  oldCentroids = centroids;
  centroids = getNewCentroids(clusters, centroids);
  if (iterations >= MAX_ITERATIONS || same(centroids, oldCentroids)) {
    return clusters;
  }

  return KMeansRecurse({
    dataset,
    centroids,
    oldCentroids,
    iterations: iterations + 1,
  });
}

function determineOptimalK(results) {
  return results
    .map(({ clusters, ...rest }: { clusters: number[]; rest: any }) =>
      Object.assign({ clusters, ...rest, silhouette: silhouette(clusters) })
    )
    .filter((f: any) => f.silhouette)
    .sort((a: any, b: any) => b.silhouette - a.silhouette)[0];
}

const KMeans = (dataset: number[]): number[] => {
  let results = new Array(dataset.length - 1)
    .fill(0)
    .map((n, K) => {
      const centroids = getInitalCentroids(dataset, K + 1);
      const clusters = KMeansRecurse({ dataset, centroids });
      const variance = getVariance(clusters);
      // console.debug({ centroids, clusters, variance });
      return { variance, centroids, clusters };
    })
    .sort((a, b) => a.variance - b.variance);
  return determineOptimalK(results).centroids
};

function getVariance(clusters: number[][]): any {
  return clusters.map((c) => stat.variance(c)).reduce((acc, c) => acc + c);
}

export default KMeans;
